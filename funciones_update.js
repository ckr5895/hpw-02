alumno={
  "num_control" :["101","102","103","104"],
  "nombre":["Cesar Hernandez Ruiz","Juan Garcia","Andres Iniesta","Carmen Aragon"]
}

grupo={
  "clave":"g1",
  "nombre":"isc",
  "alumnos":["101","102","103","104"],
  "horarios":["h1","h2","h3","h4","h5","h6","h7"]
}


horario={
  "clave":["h1","h2","h3","h4","h5","h6","h7"],
  "inicial":[7,8,9,10,11,12,1],
  "final":[8,9,10,11,12,1,2],
  "c_materia":["m1","e1","in1","hi1","ge1","ed1","et1"],
  "c_profesor":["p1","p2","p3","p4","p5","p6","p7"]
}

materia={
  "clave":["m1","e1","in1","hi1","ge1","ed1","et1"],
  "nombre":["Matematicas 1","Español 1","Ingles 1","Historia 1","Geografia 1","Educacion 1","Etica1"]
}

profesor={
  "clave":["p1","p2","p3","p4","p5","p6","p7"],
  "nombre":["Juan","Pedro","Mari","Jorge","Adriana","Oscar","Migel"]
}


function agregar_profesor(c_profesor,hora,obj_profe,obj_horario){
    var text="";
    var estado=false;
    if(hora >6 && hora <13 || hora==1 || hora==2){
        for(var i=0;i<obj_profe["clave"].length;i++){
            if(c_profesor == obj_profe["clave"][i]){
                estado=true;
                for(var j=0; j<obj_horario["inicial"].length; j++){
                    
                   if(hora==obj_horario["inicial"][j]){
                    obj_horario["c_profesor"][j]=c_profesor;
                    text="Se ha agregado al profesor "+c_profesor+" en el horario de "+hora+" a "+(hora+1);
                    }
                    
                }
            }
        }
    }
    else{
        text= "La hora de clases no existe, intenta con una clase entre las 7 am y 2 pm";
    }
    
    if(estado===false){
        text = "El profesor que intentas agregar no esta registrado...";
    } 
    return text;
}
    


agregar_profesor("p1",1,profesor,horario);


function agregar_materia(c_materia,hora,obj_materia,obj_horario){
    var text="";
    var estado=false;
    if(hora >6 && hora <13 || hora==1 || hora==2){
        for(var i=0;i<obj_materia["clave"].length;i++){
            if(c_materia == obj_materia["clave"][i]){
                estado=true;
                for(var j=0; j<obj_horario["inicial"].length; j++){
                    
                   if(hora==obj_horario["inicial"][j]){

                    obj_horario["c_materia"][j]=c_materia;
                    text="Se ha agregado la materia "+c_materia+" en el horario de "+hora+" a "+(hora+1);
                    }
                    
                }
                if(estado===false){
                    text = "La materia que intentas agregar no esta registrada...";
                } 
            }
        }
    }
    else{
        text= "La hora de clases no existe, intenta con una clase entre las 7 am y 2 pm";
    }
    
    
    return text;
}

agregar_materia("m1",1,materia,horario);

function agregar_alumno(n_ctrl,nom,obj_alumno){
    var estado=false;
    var text="";
    for(var i=0;i<alumno["num_control"].length;i++){
        if(n_ctrl==obj_alumno["num_control"][i]){
            estado=true;
        }
    }
    
    if(estado === false){
        var pos = (obj_alumno["num_control"].length+1);
        obj_alumno["num_control"][pos]=n_ctrl;
        obj_alumno["nombre"][pos]=nom;
        text="Se agrego al alumno(a) "+nom+" con el numero de control "+n_ctrl;
    }
    else{
        text="El numero de control "+n_ctrl+" ya pertence a un alumno, pruebe con otro...";
    }
    return text;
}

agregar_alumno("11","juanito",alumno);


