function eliminar_profesor(c_profesor,hora,obj_profe,obj_horario){
    var text="";
    var estado=false;
    var estado2=false;
    if(hora >6 && hora <13 || hora==1 || hora==2){
        for(var i=0;i<obj_profe["clave"].length;i++){
            if(c_profesor == obj_profe["clave"][i]){
                estado=true;
                
                for(var j=0; j<obj_horario["inicial"].length; j++){
                    
                   if(hora==obj_horario["inicial"][j] && c_profesor==obj_horario["c_profesor"][j]){
                        estado2=true;
                        obj_horario["c_profesor"][j]="";
                        text="Se ha eliminado al profesor "+c_profesor+" en el horario de "+hora+" a "+(hora+1);
                    }
                    
                }
                if(estado===false){
                    text = "El profesor que intentas eliminar no esta registrado...";
                } 
            }
        }
    }
    else{
        text= "La hora de clases no existe, intenta con una clase entre las 7 am y 2 pm";
    }
    if(estado2===false){
        text="Este profesor no da clases en ese horario -_-!";
    }
    return text;
}

eliminar_profesor("p1",7,profesor,horario);



function eliminar_materia(c_materia,hora,obj_materia,obj_horario){
    var text=":D";
    var estado=false;
    var estado2=false;
    if(hora >6 && hora <13 || hora==1 || hora==2){
        for(var i=0;i<obj_materia["clave"].length;i++){
            if(c_materia == obj_materia["clave"][i]){
                estado=true;
                for(var j=0; j<obj_horario["inicial"].length; j++){
                   if(hora==obj_horario["inicial"][j] && c_materia==obj_horario["c_materia"][j]){
                        estado2=true;
                        obj_horario["c_materia"][j]="";
                        text="Se ha eliminado la materia "+c_materia+" en el horario de "+hora+" a "+(hora+1);
                    }
                    
                }
                if(estado===false){
                    text = "La materia que intentas eliminar no esta registrada...";
                } 
            }
        }
    }
    else{
        text= "La hora de clases no existe, intenta con una clase entre las 7 am y 2 pm";
    }
    if(estado2===false){
        text="Esta materia no se imparte en ese horario!";
    }
    return text;
}

eliminar_materia("e1",8,materia,horario);


function eliminar_alumno(num_ctrl,alumno){
    var estado=false;
    var pos;
    var text;
    for(var i=0;i<alumno["num_control"].length;i++){
        if(num_ctrl==alumno["num_control"][i]){
            estado=true;
            pos=i;
        }
    }
    if (estado){
            alumno["num_control"][pos]=undefined;
            alumno["nombre"][pos]=undefined;
            pos > -1 && alumno["num_control"].splice( pos, 1 );
            pos > -1 && alumno["nombre"].splice( pos, 1 );
            text="Se elimino satisfactoriamente al alumno con numero de control "+num_ctrl;
    }else{
        text="El alumno que intentas eliminar no existe"
    }
    return text;
}

eliminar_alumno("101",alumno);
    
